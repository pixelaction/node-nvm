FROM ubuntu:latest

# declare build arguments
ARG NVM_VERSION
ARG NODE_VERSION
ARG YARN_VERSION

# install curl for installer download
RUN apt-get update && apt-get install -yq curl

# home folder for operations
RUN curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/${NVM_VERSION:-v0.35.3}/install.sh | bash

# make sure that .bashrc can be sourced in non-interactive shell
RUN sed -Ei 's/(\[ -z "\$PS1" \] && return)/# \1/' $HOME/.bashrc

# install nvm
RUN . $HOME/.bashrc && nvm install ${NODE_VERSION:-stable} && which node
RUN . $HOME/.bashrc && npm install -g yarn@${YARN_VERSION:-latest} && which yarn

# Home folder for operations
ENV BASH_ENV "/root/.bashrc"
RUN mkdir /home/workdir
WORKDIR /home/workdir
