# node-nvm docker

A docker image with nodejs from nvm.

## Features

The image accept variables in building process to easily specify
the software version(s), includes

* `NVM_VERSION`: The version string `vX.XX.X` in NVM install script download path. Default: `v0.34.0`.
* `NODE_VERSION`: The version string specified in `nvm install <VERSION>` process. Default: `stable`.
* `YARN_VERSION`: The version string for installing yarn with `npm install -g yarn@<YARN_VERSION>`. Default: `latest`.

## Registry

The publicly available image is stored in https://registry.gitlab.com/pixelaction/node-nvm

You may pull the image by:

```bash
docker pull registry.gitlab.com/pixelaction/node-nvm:<IMAGE_TAG>
```

Or you may write your Dockerfile:
```dockerfile
FROM registry.gitlab.com/pixelaction/node-nvm:<IMAGE_TAG>
```

Available image tags are:
* `4`: Based on nvm's `4` tag.
* `6`: Based on nvm's `6` tag.
* `8`: Based on nvm's `8` tag.
* `10`: Based on nvm's `10` tag.
* `12`: Based on nvm's `12` tag.
* `14`: Based on nvm's `14` tag.
* `latest`: Based on nvm's `stable` tag.

## Build your own

To build the docker image, you may simply clone this repository then build with:

```
docker build \
  [--build-arg NVM_VERSION=$NVM_VERSION] \
  [--build-arg NODE_VERSION=$NODE_VERSION] \
  [--build-arg YARN_VERSION=$YARN_VERSION] \
  [-t $IMAGE_TAG] \
  .
```

(Please see **Features** about the usage of the variables.)

## License

The source code in this repository is distributed under the MIT License.
A copy of the license is attached [here](LICENSE.md).
